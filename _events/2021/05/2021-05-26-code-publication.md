---
title: "Let's Make Your Script Ready for Publication"
layout: event
organizers:
  - belayneh
  - dworatzyk
  - "Petereit, Johannes"
lecturers:
  - belayneh
  - dolling
  - schlauch
  - "Meeßen, Christian"
  - "Lüdtke, Stefan"
type:   Workshop
start:
    date:   "2021-05-26"
    time:   "09:00"
end:
    date:   "2021-05-27"
    time:   "18:00"
location:
    campus: "Online"
fully_booked_out: false
registration_link: https://events.hifis.net/event/111/
excerpt:
    "The workshop covers topics of best practices for software publication."
redirect_from:
  - events/2021/03/16/2021-05-26and27-code-publication
  - events/2021/03/17/2021-05-26and27-code-publication
---

## Goal

This workshop aims to teach researchers step by step how existing source code can be ready for publication.


## Content

This workshop will cover the the following topics:

 - Code repository structuring
 - Minimum coding practices
 - Documentation
 - Open source licensing
 - Minimum software release practices
 - Software citation

You learn to apply the presented strategies using either the example code or your own.

## Requirements

- Basic Git skills are required. A good and quick tutorial can be found in the [Software Carpentry's Git Novice episodes 1 to 9](https://swcarpentry.github.io/git-novice/).
 - Participants require a computer equipped with a modern Web browser and their specific environment (e.g., Git client, editor) for working on their scripts.You will find more information on [the workshop page](https://swc-bb.git-pages.gfz-potsdam.de/swc-pages/2021-05-26-GFZ-virtual/).

Registration is possible from 11 to 18 May, 2021. We are looking forward to seeing you! 
