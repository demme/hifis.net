---
title: "Folding@Home and research on SARS-CoV-2"
title_image: omar-flores-MOO6k3RaiwE-unsplash.jpg
date: 2020-04-09
authors:
  - Konrad, Uwe (HZDR)
layout: blogpost
categories:
  - News
  - Use Case
excerpt_separator: <!--more-->
redirect_from:
  - news/2020/04/09/hifis-support-folding-at-home
lang: en
lang_ref: 2020-04-06-hifis-support-folding-at-home
---

The Helmholtz platform HIFIS, the HZDR and the Center for Advanced Systems
Understanding (CASUS) support the world-wide project Folding@Home in
developing an antibody based therapy to prevent a respiratory infection by
the virus.
<!--more-->
For this, a significant portion of the high performance computing capacity is
provided to assist in computing protein structures of the Corona virus. The
detailed knowledge of such structures is decisive to develop a successful
therapy.
In a coordinated effort with the computing center of the
Helmholtz-Zentrum Dresden - Rossendorf, free resources of the CASUS high
performance computer and the HZDR high performance computer "Hemera" are being
combined.

#### Interested? Comments and Suggestions?

If you are interested, please do not hesitate to contact us:

<a href="{% link contact.md %}"
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk">
                            Contact us! <i class="fas fa-envelope"></i></a>

---

#### Changelog

- 2022-04-11 Added headline picture and contact information.
