---
title: "Guidelines for Collaborative Notetaking"
title_image: glenn-carstens-peters-RLw-UC03Gwc-unsplash.jpg
date: 2020-06-26
authors:
  - Hammitzsch, Martin (GFZ) et al.
layout: blogpost
categories:
  - Guidelines
redirect_from:
  - services/collaborative_notetaking/index_ger.html
  - services/collaborative_notetaking/index.html
  - guidelines/collaborative_notetaking/index_ger.html
  - guidelines/collaborative_notetaking/index.html
excerpt:
  You are the one in charge of taking the notes - quick and precise, with
  additions from, and the consent of, all participants - and you are in charge of
  making actionable notes available for everyone immediately after the meeting.
  However, being in charge does not mean that you have to work out the notes alone.
  Instead, all participants are invited - or better even, obliged - to not only
  follow the note taking directly but also to contribute when misunderstandings,
  misinterpretations, delays in record keeping and other obstacles hinder the
  success of a joint meeting.
lang: en
lang_ref: 2020-06-26-guidelines-for-collaborative-notetaking
---

## Scope
We’re talking about _CodiMD_, _HackMD_, _Google Docs_, _Office365_,
_OnlyOffice_, _Dropbox Paper_, _Polynote_, _Quip_, _Notejoy_, _ShareLaTeX_,
_Overleaf_ and other services supporting note-taking and working at documents
in one place together with remote team members either at the same time or
asynchronously.

## Task
You're responsible for leading a group of people who work distributed in a
team - or you support someone who is. Before or after various occasions such
as meetings and presentations, the event requires agendas, meeting minutes,
notes, document drafts or even complete and final documents to enable
transparency, simplify decision-making and increase the performance of your
team.

## Situation
You are going to run an in-person meeting or more likely a conference call.
You need to keep the get-together on track with an agenda and you need to keep
a record of reported states, discussed issues, agreements and decisions,
upcoming tasks and other results.

## Challenge
You are the one in charge of taking the notes - quick and precise, with
additions from, and the consent of, all participants - and you are in charge of
making actionable notes available for everyone immediately after the meeting.
However, being in charge does not mean that you have to work out the notes alone.
Instead, all participants are invited - or better even, obliged - to not only
follow the note taking directly but also to contribute when misunderstandings,
misinterpretations, delays in record keeping and other obstacles hinder the
success of a joint meeting.

## Approach
Set up a document in advance which is accessible for all participants.
It should contain the agenda with references to the relevant material.
This includes information on collaborative note-taking, how to prepare and
what is expected from the participants by providing a set of rules for the
note-taking process.
Additionally, reserve a time slot in the beginning of the conference to make
sure to get everyone on board and communicate who is in charge of the final notes,
or at least of curating the collaborative notes.

## Options
Below are a few suggestions that might make it into the set of rules for your
team to make everyone feel comfortable and more productive during a meeting
while collaboratively taking notes or while collaboratively writing a document
together.

- **Select a tool meeting your needs**  
When selecting a web-based tool for collaborative note-taking offered by a
cloud service provider you should know the expectations of the meeting
participants, collaborators and other stakeholders, especially in terms of the
look and feel, the functionalities, and their suitability in the intended
workflow.
Furthermore, security - including authentication and authorization - as well
as data protection are non-functional requirements one should have in mind
when sensitive content or personal data will be stored in these documents.

- **Set-up the document in advance**  
Before the meeting, prepare the document to be used for collaborative note-taking:
include the intended agenda with topics, times and timeframes as well as
responsibilities and expected results. If required, refer to related documents
and other sources to allow preparation for the participants in advance and to
have everything at hand.
Leave space close to each agenda item thus indicating the place to facilitate
inline note-taking in the agenda so that the document automatically becomes the
meeting minutes with contributions from everyone attending the meeting.

- **Set-up a reusable template**  
  If the prepared document has worked in the meeting, consider the creation of a
  template to allow an efficient preparation for regular meetings.

- **Allow access and share the document**  
  Depending on the options of the cloud service provider for giving others
  access to the document, you may set different access levels, e.g., full
  editing, tracked editing, commenting, or read-access only.
  You might give access to the public, or to specific individuals who need to
  have an account with the service provider. While giving access to specific
  people protects the document from access by others, publicly accessible
  documents can be factually protected by sharing the URL address of the
  document with specific people only.
  However, even if a long and cryptic string within the URL offers limited
  protection, public documents can still be found by chance or on purpose
  through URL guessing with automated requests.
  So, in any case, remind participants not to share further the URL in case of
  confidential or sensitive content, or do not allow access to the public and
  give individual access permissions, if possible.

- **Make yourself comfortable with the note-taking tool**  
  Once you have access to a document used for collaborative note-taking, open
  it and explore the behaviour of and functions available in the tool with the
  permissions given to you.

- **Make yourself comfortable with the note-taking itself**  
  Effective note-taking requires effort and simultaneous interaction with the notes.
  Stay focused during the meeting, pay close attention and actively participate.
  This helps optimize the results from the meeting.
  So make yourself comfortable with each of the note-taking phases - they all
  require a different behaviour of individuals and the group of collaborators
  in certain stages of a meeting and of the note-taking.

- **Double-check information about you**  
  Check if your name and your affiliation are entered correctly in the
  respective field so that the host and others know who you are, can address
  you by name, and identify your contributions and comments in the document.

- **Introduce the note-taking process**  
  Collaborative note-taking requires a certain amount of discipline.
  Everyone should be aware of the intended process for taking notes together.
  So a quick recap of rules in the beginning of a meeting helps everyone to
  contribute to the note-taking process. This includes to name the responsible
  person, ask everyone else to contribute, and outline how to contribute by
  repeating the relevant rules from this list.

- **Take notes, contribute and follow**  
  If you don't speak, either listen and take notes in your own words to
  paraphrase what you understand, or, if you don't write, follow other people’s
  notes, review them, and comment them according to how you have understood
  the subject matter.

- **Provide immediate feedback and clarify discrepancies**  
  When you identify different understandings while taking notes together with
  others, then don’t feel shy and speak up to clarify them directly in the
  meeting and keep track of the clarified understanding in the notes immediately.
  Speaking up could mean adding a comment in the notes to be taken up by
  the host or other participants to clarify. Also, take up comments of others
  and clarify different views, either by correcting, e.g. typos or misspellings,
  by commenting, or by chatting in the note-taking tool, or - if this doesn’t
  solve a different understanding - by initiating a short discussion directly
  in the meeting.

- **Ask information and questions for further discussion**  
  If necessary, add further information not yet discussed or add questions to be
  discussed so that these information can be taken up by the host in the further
  discussion of the meeting.

- **Stay structured and organized**  
  Take notes for important, relevant, and non-trivial information, for
  decisions, for tasks, upcoming events and anything else that will be of any
  help personally for you at a later point of time thus allowing you to retrace
  and understand what was reported, discussed, and decided - and why.
  Write in phrases, not complete sentences, use bullet points and lists where
  possible, and develop a consistent system of abbreviations and symbols
  together with the others to save time as you take notes.

- **Earmark and highlight take-home messages**  
  The use of key words and colours are options to label and emphasize specific
  items that are important for the group and easy to spot throughout the document.
  Often this is group-specific and must be negotiated within a team beforehand,
  so that everyone deals with it in the same way.
  Take cues to apply labels, e.g. “task”, “todo” or “decision”, keep priorities,
  e.g. “important” or “postpone”, define deadlines, e.g. by giving dates,
  define responsibilities, e.g. by naming persons, and use highlighters and colours,
  e.g. to indicate key ideas, changes in concepts or links between information.
  Use these earmarks and highlights at the end of the meeting to go through and
  wrap-up the important take-home messages. If necessary, allow for some time
  to work on the notes or discuss them again in the meeting until all agree
  with the notes which represent your results.

- **Finalise the notes**  
  Reserve time to finish the notes immediately after the conference call or
  in-person meeting when discussions, results and other things said can be
  remembered more easily.
  Finish bullet points that may have been left incomplete or misleading.
  Order items if necessary. Double-check wording, spelling and grammar.

- **Share the notes and ask for corrections**  
  Finally, share the polished version of the document with all participants
  providing a fixed time frame for possible feedback, corrections, and comments.
  Provide a clear procedure for adding changes and comments so that they can be
  traced subsequently.

- **Accept final contributions, freeze and share the document**  
  Integrate subsequently provided feedback, corrections, and comments, finalise
  the document, and make the final version available for the target group after
  freezing the document so that no changes can be applied further on.
  Freezing a document can be realised by creating a separate PDF version of the
  document and integrating an appropriate comment or reference to the final
  version in the original note-taking document.
  In any case and if possible, freezing a document should additionally be
  realised by withdrawing permissions for editing the original document in the
  web-based tool for collaborative note-taking offered by the cloud service provider.

_These recommendations have been created in collaboration with
Stephan Druskat (DLR), Bernadette Fritzsch (AWI), Maryna Bondarava (HMGU) and
Thomas Schnicke (UFZ)._

<div class="alert alert-success">
  <h2 id="contact-us"><i class="fas fa-info-circle"></i> Comments or Suggestions?</h2>
  <p>
    Feel free to <a href="{% link contact.md %}">contact us</a>.
  </p>
</div>
