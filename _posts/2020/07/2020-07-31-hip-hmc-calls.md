---
title: "Project Calls for HIP and HMC"
title_image: default
date: 2020-07-31
authors:
  - jandt
layout: blogpost
categories:
  - News
excerpt_separator: <!--more-->
---

Two of our fellow Helmholtz Incubator platforms (HMC and HIP) recently announced their project calls.
Make sure to not miss the respective deadlines in September and October!
<!--more-->


#### Helmholtz Imaging Platform
The [Helmholtz Imaging Platform (HIP)](https://www.helmholtz-imaging.de/) published their first call to facilitate innovative approaches focusing on imaging problems arising in all Helmholtz research areas.

Full information on the call can be found on [HIP's overview page](https://www.helmholtz-imaging.de/our_services/overview/). 
**Application deadline is 11 September, 2020**.

#### Helmholtz Metadata Collaboration
Further, the [Helmholtz Metadata Collaboration (HMC) platform](https://helmholtz-metadaten.de/) 
put out their first call for HMC projects. 
This call aims for applications addressing practical challenges in the field of metadata generation and data enrichment.

**Application deadline is 14th October, 2020**.



<div class="alert alert-success">
  <h2 id="contact-us"><i class="fas fa-info-circle"></i> Comments or Suggestions?</h2>
  <p>
    Feel free to <a href="{% link contact.md %}">contact us</a>.
  </p>
</div>
