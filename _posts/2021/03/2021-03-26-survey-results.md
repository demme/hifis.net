---
title: "Results of the first Software Survey"
title_image: default
date: 2021-03-26
authors:
  - huste
layout: blogpost
categories:
  - News
excerpt: >
    Data from 467 respondents of the HIFIS Survey 2020 were analyzed to give
    an overview of research software developers at Helmholtz and their
    day-to-day work. While software development plays a crucial role in all
    six research domains, only about half of the respondents are satisfied
    with the support at their centers. Results of this survey will help us
    to better understand how HIFIS Software Services could improve this
    situation.

---

# Analysis of First HIFIS Software Survey  

{{ page.excerpt }}

#### Detailed Results

The results of the HIFIS Software survey 2020 are published in four posts
examining the survey from four different perspectives:

* [**A Community Perspective**]({% post_url 2021/01/2021-01-21-survey-results-community %})
* [**A Consulting Perspective**]({% post_url 2020/12/2020-12-16-survey-results-consulting %})
* [**Programming, Continuous Integration and Version Control System**]({% post_url 2020/11/2020-11-27-survey-results-language-vcs %})
* [**A Technology Perspective**]({% post_url 2020/11/2020-11-27-survey-technology %})

#### Upcoming Second HIFIS survey 

HIFIS is preparing a Helmholtz-wide survey, which will focus on the
usage and user requirements in the area of Cloud services in the context of
research software development.
This survey builds on the experiences that the HIFIS Software Cluster has
already made with a survey at the end of 2019/beginning of 2020,
and at the same time broadens the perspective.

To not miss any updates on this and other HIFIS progress:

<a href="https://lists.desy.de/sympa/subscribe/hifis-announce" 
                            class="btn btn-outline-secondary"
                            title="Subscribe to HIFIS Announcements Letter - also unsubscribe"> 
                            Subscribe to HIFIS Announcement letter<i class="fas fa-bell"></i></a>
