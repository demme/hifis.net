---
title: "HIFIS Homepages unified"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
data: 2021-05-05
authors:
  - jandt
  - huste
  - spicker
layout: blogpost
categories:
  - News
excerpt: >
    The HIFIS pages have been re-united! Yay! 
    After some time of living separately, the former software.hifis.net and hifis.net homepages have been merged to one, as they should be. 
    All HIFIS services - covering software, cloud, backbone and overarching services, are now being presented on one unified page. Check it out!

---

{{ page.excerpt }}

#### Central elements of the new page
All details, documents and explanations on HIFIS are accessible from [hifis.net]({% link index.md %}), with some being directly accessible under the following shortlinks:

* [**hifis.net/services**]({% link services/index.md %}) -- All HIFIS Services in a nutshell.
* [**hifis.net/events**]({% link events.md %}) -- HIFIS Events, including Courses and Community Events.
* [**hifis.net/news**]({% link news/index.html %}) -- HIFIS News and Announcements.
* [**hifis.net/publications**]({% link publications.md %}) -- Publications of HIFIS.
* [**hifis.net/roadmap**]({% link roadmap/index.md %}) -- HIFIS Roadmap.
* [**hifis.net/doc**](https://hifis.net/doc) -- HIFIS Technical and Adminstrative Documentation.

#### Forwarding of old software.hifis.net links

All links to the former software.hifis.net page are being redirected to the new site, so don't fear to end up in missing links all the way.
However, you are encouraged to update any dangling links to the new site, when possible.

#### Stay tuned!

This site will be continuously updated. We will add anything that's new on topics like

* New Services provided,
* Use Cases of Services,
* Frequently Asked Questions,
* ...any many more!

To never miss any updates on HIFIS, we invite you to:

<a href="https://lists.desy.de/sympa/subscribe/hifis-announce" 
    class="btn btn-outline-secondary m-1"
    title="Subscribe to HIFIS Announcements Letter - also unsubscribe"> 
    Subscribe to our Announcements Letter <i class="fas fa-bell"></i>
</a><a class="btn btn-outline-secondary m-1" href="{{ '/feed/news.xml' | relative_url }}"
    title="Subscribe to HIFIS News RSS Feed (RSS Reader required)"
    type="application/atom+xml"
    download> 
    Subscribe to our News Feed (RSS) <i class="fas fa-rss"></i></a>

#### Comments and Suggestions

If you have suggestions, questions, or queries, please don't hesitate to write us.

<a href="{% link contact.md %}" 
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk"> 
                            Contact us! <i class="fas fa-envelope"></i></a>
