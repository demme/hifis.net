---
title: "Report on the TEACH Conference in 2021"
title_image: default
date: 2022-02-14
authors:
  - Ehrmanntraut, Sophie (HIDA)
  - Erxleben, Fredo (HIFIS)
  - Ferguson, Lea Maria (Helmholtz Open Science Office)
  - Florian, Mona (Helmholtz Talent Management)
layout: blogpost
categories:
  - News
  - Report
  - Education
  - Event
excerpt: >
  This is the report on the proceedings and outcome of the TEACH (Talk About Education across Communities in Helmholtz).
---

## About this document

This report documents the organization, proceedings and evaluation of the TEACH conference.

## General information on the event

### Motivation
This event has its roots in the various Helmholtz platforms that are being developed for a few years now, and the Helmholtz Open Science Office. These organizations hold comprehensive vantage points spanning across the various centers in Helmholtz. Since the early stages, when the involved stakeholders were mostly concerned with setting up their respective infrastructures and services, it became very apparent that the communication across the various centers is a key element for the Association. During those initial contacts it stood out that the platforms and the OS Office came with one or even multiple education and community-related work packages included. This led to the insight that collaboration across centers is fruitful and necessary to enhance teaching and education, albeit in different domains. This insight spurred the idea to set up a networking event for trainers, educators, and personal developers to exchange ideas and to connect. As a result, an event was brought to life with the title _TEACH - Talk about Education Across Communities in Helmholtz_ which fittingly describes a central issue that binds all contributions together.

### Proceedings
TEACH was held from December 7-10, 2021 via the online platform _GatherTown_. 
In the four days of the event, more than 100 educators, trainers, and personnel developers of the Helmholtz Association shared their ideas and insights on teaching and education in 19 participant-driven sessions provided by 42 individual contributors. 
These sessions were framed by an opening and a closing session that provided the overall vision of the event, as well as further information resources and networking possibilities for all participants. 
Additionally, a poster space allowed for the presentation of the participating platforms and organizational units as well as a support letter by the President of the Helmholtz Association, Otmar D. Wiestler. 

## Takeaway messages from the proceedings

While the scope of the conference was intentionally kept broad to allow for as many various ond open contributions as possible, several key points were repeatedly made by contributors and participants alike:

* The communication within Helmholtz is perceived as one of the biggest challenges for a more effective outreach and coordination of the various education programs. This is not limited to the interaction between Centers but also within the Centers itself.
    * In many cases, educators and organizers have to rely on personal contacts to enable their offers to reach the intended target groups or pull knowledge on work-related topics. This increases the workload and reduces the efficiency with which educational offers can be provided.
    * Under the same heading it was mentioned that short contract terms tend to disrupt built-up acquaintances since the inherent abilities, further contacts, and specific knowledge usually leave together with the affected persons. In turn, the installment of effective education programs can be severly set back by such dynamics.
    * A dedicated internal communication office was suggested for Center-internal communication to disseminate information quickly, and well-targeted, establish contacts between parties with shared interests and re-establish such contacts if positions within the Centers undergo personal changes.
* The demand for education within Helmholtz is immense. The requirements for early-career graduates and post-doc researchers are steadily increasing, notably in the areas of digitalization where they often only tend to bring marginal skills from their previous education. 
* Onboarding new employees, especially young scientists often still faces too many challenges. They are often not sufficiently aware of educational offers which could benefit them especially early in their career and are also often out of reach for educators.
* Besides the scientific staff also technical and administrative staff should be trained to a certain extend in approaches which can improve their workflow (e.g., version control, using markdown documents, communication via instant messaging services). This can also serve to ease interactions between scientific, administrative and technical staff, since it allows them to all interact on par with respect to the tools used and allows the creation of workflows that can overarch multiple domains of the employees' work life. Data stewards and data librarians in the libraries of the Helmholtz Centers play vital networking roles in this regard.
* The required amount of skills and knowledge to keep up with the best practices and ever modernizing technology keeps steadily increasing and current efforts will not be sufficient in the long run.
    * Dedicated Research Software Engineers are in increasing demand, which will continue to grow.
* Switching to online education has been a challenge for many educators and organizers. Currently, most could establish a working modus operandi with room for improvements. TEACH provided a much needed opportunity to exchange best practises and didactic approaches as well as evaluate some commonly used tools.
    * Online teaching is here to stay. It is harder to do right when compared to in-person teaching but offers great boons with regards to work-life balance and can enable a more self-directed learning. Along with the technical aspects the didactic approaches are forced to evolve as well, which is expected to also be reflected in the way in-person workshops will be held in the future. 

## Evaluation of organizational aspects

The organization of TEACH mainly was mainly done by the respective authors. Further support was provided by HMC which provided a _GatherTown_ space that could support up to 100 participants at the same time. An [_Indico_ instance][hifis-events] provided by HIFIS was used for event registration, participant- and contribution management. This is also where all materials of the contributions can be found.
Communication within the organizers group was facilitated via [_Mattermost_][hzdr-mattermost] which is accessible for all members of Helmholtz via _Helmholtz AAI_. 

The timetable was split into 90-minute blocks with half-hour breaks between them (one full hour for the lunch break). This organizational approach was chosen to combat the, by now well-known, effect of "Zoom-fatigue", which leads to a strong deterioration in mental focus when exposed to long video conference calls.

### Choice of platform
_GatherTown_ is an unusual platform in the sense that it maps participants onto avatars in a virtual space which mirrors some experiences from real live (e.g., being able to move around, getting close to someone to establish a conversation, being able to speak and listen directly to persons in one's vicinity or from a stage) to this virtual setting. It takes a gamified approach to interaction and video conferencing, reminding some of an old-school computer game.
External resources like posters and websites can be linked direktly onto objects in the virtual space, allowing for easy interaction by the participants.

It was made sure that organizers were visible among the participants, easy to reach and a in-platform helpdesk was provided in case participants experienced issues. The overall stability of the conference platform was very good and occasional issues stemmed from the internet connection on the participants' side.

A preparation event was held to give contributors the opportunity to get to know the platform and help them with planning their contributions.

## Reception and Feedback

The rather informal and broad style of the event was well appreciated by the participants and contributors. The selection of topic was perceived as interesting, which was dimished somewhat by the tight schedule. The conference was considered by some to not be sufficiently long to address such a complex topic.

A majority of participants stated that they enjoyed _GatherTown_ as a new, fresh and interactive approach. Most participants quickly caught on to the unfamiliar controls and used the self-determined movement option to explore the various spaces and items within. The posters were quickly discovered and provided good conversation starters.
While the somewhat playful visuals appealed to most, individual opinions do disagree with the style and presentation. 
It was noted that _GatherTown_ as a service is hosted by a company in the United States. An open-source alternative is available in the product WorkAdventure which could be self-hosted by an instance in Helmholtz. It was stated that multiple parties would prefer such kind of video conferencing system for informal work settings or workshops. At the time of writing, HIFIS has taken first steps to evaluate the feasibility of providing a platform of this kind to the Helmholtz community.

## Lessons learned
The organizers have extracted several action items from the feedback (see also _Reception and Feedback_ below): 

* Do not hold such an event near the end of the year. Multiple participants noted that they had to cut their visits short due to tight schedules. A possible repetition for the event will be scheduled for autumn in the future.
* Seperate the tracks more clearly to allow participants an easier choice concerning their interests. Keep the overall broad scope of conference though.
* Have contributions run in parallel. Due to the fully packed schedule some interesting discussion had to be cut short which should not limit such a creative and communicative event. Overlapping talks on different stages can give singular events more room to unfold and allow contributors zo interact more with the participants. Also plan more time for the handover between contributions.
* Plan explicit networking sessions. Networking happened often in the breaks between contributions, defeating their purpose as periods of rest to give participants time to recouperate. As communication and networking turned out to be of utmost importance, it should be more in the spotlight in future events.
* Not all contributors could participate in the preparation event which made the planning less predictable for them. For future iterations the organizers will have to interact closer with contributors to ensure they can utilize the provided platforms full potential and have a better insight wich interactions the participants can be expected to be familiar with. Allowing for a longer period for registration and contributions should be planned to allow contributors to get to know their participants better beforehand.
* Advertisement of the TEACH was considered insufficient, with many participants learning about it by chance. Building up stronger communication networks beforehand is vital.

## Thanks
The organizers of the TEACH would like to thank all contributors and participants without whom this would have been a rather dull affair.
Thanks also go out to all our colleagues in HIDA, HIFIS and HMC who keep the services running upon which the event was built, who gave valuable input and feedback and helped out with the thousand small tasks that such an event entails.

[hifis-events]: https://events.hifis.net/event/164
[hzdr-mattermost]: https://mattermost.hzdr.de
[twitter-teach]: https://twitter.com/hashtag/LetsTEACH
