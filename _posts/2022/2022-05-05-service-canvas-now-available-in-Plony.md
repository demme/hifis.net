---
title: "Service Canvas now available in Plony!"
title_image: background-cloud.jpg
date: 2022-05-05
authors:
 - holz
layout: blogpost
categories:
  - News
  - Announcement
  - Cloud Services
excerpt: >
  At the end of April, the Service Canvas in Plony was moved to our production system.
---

## Service Canvas now available in Plony!

**At the end of April, the Service Canvas in Plony was moved to our production system**

We are happy to announce that the Service Canvas is now available in [Plony](https://plony.helmholtz-berlin.de/)!

### A Short recapture of the Onboarding Process: 
- In order to hand in a Service Application a Service Provider first needs to fill out the [Application Form](https://plony.helmholtz-berlin.de/sc/service-application) in Plony, thus giving some basic information on the service to check whether a service fulfills the required exclusion criteria (see service [selection criteria list](https://hifis.net/doc/process-framework/0_Corresponding-files/HIFIS-Cloud-SP_Service-Selection-Criteria-v2.01.pdf)).
- If the service fulfills all requirements, it joins the Helmholtz Cloud Service Portfolio. To drive the Service Onboarding, more detailed information about  service is required which is collected via the **Service Canvas**.
    - Please remark that the service information collected in Plony are not public. Only dedicated information are transferred to Helmholtz Cloud Portal and therefore visible in public.
- The Service Canvas includes weighting criteria which determine the service’s rank in the Service Integration list – the services with the highest rank will be integrated first.
    - The Service Integration Team will get back to the Service Provider as soon as the Service (Canvas) Information was checked.

### Features of Service Application and Service Canvas Form in Plony:
-	Connected to Helmholtz AAI – simply Login with credentials of home institution
-	Flexible in use – one can start filling the Service Canvas, save it and continue at any time without losing information already typed in
-	Guiding through mandatory fields – validation functionality allows to check whether all mandatory fields are filled out

### Features of Service Canvas specifically:
- Automated score calculation – Plony calculates the score earned through weighting criteria automatically and shows the results for each service after having sent the Service Canvas to HIFIS
- Transparency in every status – using the overview “My Services” the Service Provider can see their services sorted by status. To improve transparency about services within a particular Helmholtz center, all services are visible that were handed in by colleagues from the same Helmholtz center.

For the Services which are part of the [Service Portfolio](https://hifis.net/doc/service-portfolio/service-portfolio-management/current-services-in-portfolio/) and which are not yet listed in Plony, we will soon transfer the collected Service Information to Plony to have all Service Information in one place.

## The next step to have a fully integrated Onboarding Process in Plony is already ahead!

The HIFIS Cloud team is implementing the interface to Helmholtz Cloud Portal next – as soon as this is done, the whole Onboarding Process is built in Plony. Service Information can then be managed in Plony, even during Service Operation.

