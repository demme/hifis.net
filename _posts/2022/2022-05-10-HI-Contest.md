---
title: "Best Scientific Imaging—the choice is yours!"
title_image: blue_curtains.jpg
date: 2022-05-10
authors:
 - klaffki
layout: blogpost
categories:
  - News
  - Announcement
  - Helmholtz Imaging
excerpt: >
  Everyone can vote in the Public Choice Award for the Best Scientific Imaging until Sunday, 29.5.2022.
---

### Outstanding images—discover structures, colours and tiny worlds
Scientific Imaging does not only lead to research results, but also to beautiful, surprising, aesthetic images. Our partner platform [Helmholtz Imaging](https://www.helmholtz-imaging.de/) therefore called for images from any imaging technique developed or applied at Helmholtz Centers. 

### Have a look at last year's winner to inspire curiousity

{:.treat-as-figure}
{:.float-left}
The architecture of nerve fibres of a monkey's cerebellum, revealed by Polarized Light Imaging—Forschungszentrum Jülich / Markus Axer et al.
![multicoloured organic structure]({{ site.directory.images | relative_url }}posts/2022-05-09-hi-image-contest/Markus_Axer_TheMonkeysFiberArchitectureReveleadByPolarizedLightImaging.jpg "multicoloured organic structure")

To give you an impression of the outstanding images that await you, here's last year's winning image: Markus Axer and his team from the Forschungszentrum Jülich won with this multicoloured image of a monkey's cerebellum, using 3D polarized light imaging (3D-PLI). This technology allows to capture the architecture of nerve fibres and was developed at the Institute for Neuroscience and Medicine.

### The choice is yours!
A jury curated a shortlist of amazing pictures and asks the public now to vote for the winners in the Public Choice Award for the Best Scientific Imaging. 
You can vote for five pictures (one vote per entry) until Sunday, 29 May 2022 [right here](https://awards.helmholtz-imaging.de/entry/vote/lgpGbmxR).
The winners will be announced at the [Helmholtz Imaging Conference 2022](https://www.helmholtz-imaging.de/article/helmholtz_imaging_conference_2022/index_eng.html), which takes place in Berlin from 31 May till 1 June 2022.



