---
title: "ODV—A Cloud-based Haven for your Georeferenced Data"
title_image: branimir-balogovic-fAiQRv7FgE0-unsplash.jpg
data: 2022-08-10
authors:
  - "Mieruch-Schnülle, Sebastian"
  - klaffki
layout: blogpost
categories:
  - Use Case
tags:
  - Earth & Environment
excerpt: >
    Text on ODV
---

## Headline

{:.treat-as-figure}
{:.float-left}
![colourful 0s and 1s forming a funnel]({% link assets/img/spotlights/odv_plus/odv5-logo.jpg %})

Texts

Texts

Texts

## Get in contact
For ODV: Sebastian Mieruch-Schnülle, sebastian.mieruch@awi.de

For HIFIS: Uwe Jandt, support@hifis.net
