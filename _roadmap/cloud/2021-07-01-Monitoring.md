---
date: 2021-07-01
title: Tasks in Jul 2021
service: cloud
---

## Usage and Success Monitoring of Helmholtz Cloud Services
The success of the services is essentially reflected in their use and application. From the middle of the year, key figures on the use of the individual services are therefore determined and collected. 

