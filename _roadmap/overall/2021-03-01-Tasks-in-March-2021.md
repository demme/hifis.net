---
date: 2021-03-01
title: Tasks in March 2021
service: overall
---

## Finish reporting for the first two years of HIFIS

After the ramp-up phase during the first year, the initial Cloud, Backbone and Software Services are 
expected to be in production by early 2021. We will report on the integration progress and impact of these
services, especially with respect to user acceptance and added value for the scientific community.
