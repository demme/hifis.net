---
date: 2020-09-01
title: Tasks in September 2020
service: software
---

## Launch of the HIFIS Software Consulting Services
HIFIS Software offers free-of-charge consulting as a service to research groups within
the Helmholtz umbrella.
We can help you deal with specific licensing issues, migrating code between
different environments and languages, setting up new projects, and other
problems that you need to solve.

<i class="fas fa-info-circle"></i> [Request consulting.][consulting-page]

[consulting-page]: {% link services/software/consulting.html %}
