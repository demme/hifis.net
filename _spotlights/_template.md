---
layout: spotlight

###############################################################################
# Template for Software spotlights
###############################################################################
# Templates starting with a _, e.g. "_template.md" will not be integrated into
# the spotlights.
#
# Optional settings can be left empty.

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------

# The name of the software
name:

# The date when the software was added to the spotlights YYYY-MM-DD
date_added:

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image:

# One or two sentences describing the software
excerpt:

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image:

# Title at the top, inside the title-content-container
title:

# Add at least one keyword
keywords:
    -

# The Helmholtz research field
hgf_research_field:

# At least one responsible centre
# Please use the full and official name of your centre
hgf_centers:
    -

# List of other contributing organisations (optional)
contributing_organisations:
    - name: # Name
      link_as: # Link (optional)

# List of scientific communities
scientific_community:
    -

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact:

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type:
      link_as:

# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license:

# Is the software pricey or free? (optional)
costs:

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    -

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    -

# List of programming languages (optional)
programming_languages:
    -

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
# If you have one doi, please provide it as a variable:
doi:
# If you have multiple products, e.g. in a framework, and you want to reference each product with a DOI,
# then please provide a dictionary. Uncomment the following lines:
#     - name: # Product name for first DOI
#       doi: # DOI


# Funding of the software (optional)
funding:
    - shortname: # Abbreviation
      funding_text: # Short text or sentence, if required by your funding guidelines (optional)
      link_as: # Link (optional)
---

# Markdown

Here is space for some custom markdown.

Please provide images always with a footnote like this:

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/PATH_TO_IMAGE">
<span>Descriptive sentence.</span>
</div>
