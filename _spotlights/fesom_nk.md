---
layout: spotlight

# Spotlight list attributes
name: FESOM
date_added: 2021-12-17
preview_image: fesom/FESOM2_logo-RGB_72dpi_iceblue.png
excerpt: FESOM (Finite-Element/volumE Sea ice-Ocean Model) is a multi-resolution sea ice-ocean model that solves the equations of motion on unstructured meshes.
# Title for individual page
title_image: FESOM-Eddies.jpg
title: FESOM (Finite-Element/volumE Sea ice-Ocean Model)
keywords:
    - ocean modelling
    - oceanography
    - unstructured meshes
    - variable resolution
    - climate modelling
    - High Performance Computing
    - exascale
hgf_research_field: Earth & Environment
hgf_centers:
    - Alfred Wegener Institute for Polar and Marine Research (AWI)
contributing_organisations:
scientific_community:
    - Earth System Modelling
impact_on_community: used in AWI Climate Model AWI-CM which contributes to CMIP6
contact: fesom@awi.de
platforms:
    - type: webpage
      link_as: https://fesom.de/
    - type: mailing-list
      link_as: https://www.listserv.dfn.de/sympa/info/fesom
    - type: github
      link_as: https://github.com/FESOM/fesom2
license: LGPL3
costs: Free
software_type:
    - Modelling
application_type:
    - HPC
programming_languages:
    - Fortran, C++
doi: 10.5194/gmd-10-765-2017
funding:
    - shortname: AWI
    - shortname: DFG
    - shortname: EU
---

# FESOM

FESOM is the first mature global sea-ice ocean model that employs unstructured-mesh methods. It was developed mainly for usage in large scale ocean circulation and climate research.

Unstructured meshes allow to flexibly increase resolution in dynamically active regions, while keeping a relatively coarse-resolution setup elsewhere. FESOM allows global multi-resolution simulations without traditional nesting.

FESOM is created for HPC machines, and with this increase in numerical efficiency, the performance of FESOM2.0 is very close to that of structured-mesh codes. This allows to efficiently run simulations on the kilometer scale.

It is the ocean sea ice component of the coupled climate model AWI-CM, which contributes to CMIP6, as well as several other coupled systems.

See our [key publications about FESOM2](https://fesom.de/models/fesom20/), as well as [related publications](https://fesom.de/publications/).

## Background

The latest version of FESOM places scalar degrees of freedom at triangle vertices (nodes), and the horizontal velocity is on elements (triangle centroids). In the vertical, most of the variables are stored at mid-layers.

The code solves the standard set of equations derived under the standard set of approximations (Boussinesq, hydrostatic, and traditional approximations). These equations include the momentum equation for horizontal velocity, the hydrostatic equation, the Boussinesq form of the continuity equation and the equations for potential temperature and salinity.

FESOM is equipped with the set of parameterisations that allows to run configurations from very low (for paleo applications) to very high (for Digital Twin applications) resolutions. It also includes sea ice model.


<div class="spotlights-text-image">
    <img src="{{ site.directory.images | relative_url}}spotlights/fesom/FESOM-Grid.jpg" alt="Image of unstructured grid in FESOM2.">
    <span>FESOM2 uses unstructured grids with increased resolution in dynamically active regions.</span>
</div>
