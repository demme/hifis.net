---
layout: spotlight

# Spotlight list attributes
name: GeoStat-Framework
date_added: 2022-07-05
preview_image: geostat/GeoStat.png
excerpt: >
    The GeoStat Framework is a coherent ecosystem of Python packages for
    geostatistical applications and subsurface simulations. It provides an 
    easily usable open source collection of software packages. They are well 
    documented, including exhaustive hands-on guides and examples for 
    helping non-programmers and non-domain-experts getting started.

# Title for individual page
title_image: GeoStat.png
title: GeoStat-Framework
keywords:
    - python
    - geostatistics
    - kriging
    - covariance-model
    - srf
hgf_research_field: Earth & Environment
hgf_centers:
    - Helmholtz Centre for Environmental Research (UFZ)
contributing_organisations:
scientific_community:
    - Geostatistics
impact_on_community:
    - tools for geostatistic modelling in Python
contact: info@geostat-framework.org
platforms:
    - type: webpage
      link_as: https://geostat-framework.org
    - type: github
      link_as: https://github.com/GeoStat-Framework
license: 
    - LGPL-3.0-or-later
    - MIT
costs: Free
software_type:
    - modelling packages
application_type:
    - Desktop
programming_languages:
    - Python
doi:
    - name: GSTools
      doi: 10.5194/gmd-15-3161-2022
    - name: ogs5py
      doi: 10.1111/gwat.13017
    - name: welltestpy
      doi: 10.1111/gwat.13121
    - name: AnaFlow
      doi: 10.1016/j.advwatres.2021.104027
    - name: pentapy
      doi: 10.21105/joss.01759

funding:
    - shortname: UFZ
      link_as: https://www.ufz.de/
    - shortname: DBU
      link_as: https://www.dbu.de/stipendien_20016/432_db.html
---

# GeoStat-Framework
<p align="center">
<a href="https://geostat-framework.org/">
<img src="{{ site.directory.images | relative_url}}spotlights/geostat/GeoStat.png" alt="GSFrame-LOGO" width="251px"/>
</a>
</p>
<p align="center"><b>Create your geo-statistical model with Python!</b></p>
<p align="center">
<a href="https://github.com/GeoStat-Framework/GSTools/discussions"><img src="https://img.shields.io/badge/GitHub-Discussions-f6f8fa?logo=github&style=flat" alt="GH-Discussions"/></a>
<a href="https://swung.slack.com/messages/gstools"><img src="https://img.shields.io/badge/Swung-Slack-4A154B?style=flat&logo=data%3Aimage%2Fpng%3Bbase64%2CiVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABmJLR0QA%2FwD%2FAP%2BgvaeTAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAB3RJTUUH5AYaFSENGSa5qgAABmZJREFUSMeFlltsVNcVhr%2B1z5m7Zzy%2BxaBwcQrGQOpgCAkKtSBQIqJepKhPBULpQ6sKBVWVKqXtSy%2BR0qYXqa2qRmlDCzjBEZGKUCK1TWqlNiGIEKDQBtf4Fki4OIxnxrex53LOXn2YwbjEtOvlHG3tvX%2Btf%2B21%2Fl%2BYJ1QVEbn1vwLYBWwCVgG1lW0ZoA%2FoAQ6LSP%2BdZ%2BeGzAMiIqK%2Bem0GpxNYVeBj3j2b4NCfM2QnfAAaa11al4fZuCZK24owQJ9v%2BbLryIVbd9wVSNUaEWNVtQPYfXHmAD0T32ZJeBM1Q8d0zzMDUpMwAFgLJU%2BxClURw9NfqedLWxMAHSKyR1WNiNhPAM0B6c%2FbdPORTLuOeUMSNkmMBHgyeo32bwwRDMh8bDM%2BZVl0j6uvPrdYknFnSESWzwUzt%2BkyVlUHx7zh5j%2BmPkXBjosjLkWdominiMQ%2BoiEZxuq8OFRXGXJ5K5%2Fde5nha8VlqjooIlZVBcBUiqeqemjGppd1ptfhSpS8pmmN7GVf4whPNY4Di9m%2BMcR03nK3sBbCQeFbv7gBsExVOyp3l6nz1VtjcM4fTK3Uok5IXtPsrHuPevcBXk8d4dWPX6I%2BsIB9wf1s%2B2Y%2FVbFynUIBIeDeplIECiXl5Iv3kbLogogRgbWukfNumT%2FnlYszBxj3hwXg0cQvqXcfYNu5tVyYPE%2B1G8dXn%2BfW72fH49U8sSlOPGr4SccoF4cKs3WzFrY%2BFCMUNmz%2Ba0aeWR1l15JwJ7DaVPpk1YnJ7xIxtQRNjDXRvTx%2F9ef0Tl0g6SYQhAlvmkH%2Fgv74qUaiTSG8ewJ0%2FGgRK5aG8Cts5ouWDa1RxoDRovK9i9MAq1S12QA7b5ROUdBxBIeQ1ACG49m%2FEXPis7Qk3ChHbx6Qw1dgXVeWB7uyDOctP%2Fx6w2zdrIVIyFCyiq8wXlJOZzyAXQbY%2FGGhC8EAilJ%2BVg7ufxU6IAHeSvewfQEadiDuCr%2B6NE1LU4hwUFAF1xFGRkvEjVDlgiPwVqoEsNkAq0ZKp3EIYrFM2xGm7Uc8u%2FzXjHkTmHIHoCiDM73E3IIsDCtRV3gn7QHQ0hTCt0ooKLw%2FWCAM1AcNISOcHSsBrDRAbc7eQMQBFFciHM18kaZIMz3r%2F0HO5mazytsiw%2FmTtCYiGGCkQlltwkEVjMDVmyUA6oIGR%2BDGjAWoM3f2giHAhH%2BFI5nPsDrWxqWNE9S4tUz5k1S7cQ5df4k9S6qY9JRipXtr4w5WQYH0eHkWrqxy8FTn3AvpmFmIqj%2B76EiQjNfHH1JNWFKc3vABj9V9npw%2FRXfmBNsaoTRnRAQDAgqqMJr1KBWUtUmHaR8WRgzAqAH6FgYexqd4R2Yuns5wcLSFK4U36bj%2FdbbUbGdoZoCi3uS%2Bqtt73TlNWygpqXGfZTGXnKesrwkA9BmgZ0noMZT5R0tQ4hzLfo4rhS46W%2F%2BCAn3T7%2BhDySiWMl2RkHArP8dAesKjPixYVbbUBwB6DHB4QWADIamuHPtkhE0t3ZP7ANhe9zgvXP2dfK0pymRJmQLiEYNW6mEVljYGuDzlkwwaHq51AQ4bERkAetvjP2XCT6H480AJeZsB4N7QYt7OnuSROtRXJV2wNNS4qIJvlbUtERJxhxcv5%2FlNWwygV0QGyzKBv%2FP%2ByFfZXf%2ButoR3UuXcS95mKNgxSjpN3qZZFHwUgFPjx5n2c9wo9ktrtcOZtMeWB2NEw4b2thivPLuIS1M%2BAzmrTy4O4ys7Zv1B5fsnVdWCr7PxYf7vej73ex2YeU1VVY9nu7ShG63vRo%2Fe%2FK1%2B518FbXkjo3OjO1XU2LFRzRZ9VdWDczFQ1VsCOHgpd1G%2FcG6jHrj2vPbn%2BjVdHNfr%2BRH92eXva2MPuvxEQpe%2BHdEnzm%2FQf4%2BrRo%2BldMUbGd393oS2dWU0cDSlw1OequrALVG9Q8rLsquqg2OlzLL2Myu1N5eShgB4CjEnSMSJYrX8Oj0t8UH7NMnX0iSDwmhBWRl3tKs9IcmgGRSRZqtqzFwpL4uWWKvWiMjyZKC24%2F1HbsrLn95Pwk3gCpS0yIw%2Fg6clPC2RLc3QmzvJupoARQsvrItxZmtSkkFz6E6Q%2F2m3PFta44jbCaw%2BO3GK7uybnJs8xfXC1fLYCdTz9NIfsCS0mYVhAHp9ZYdr5J%2F%2F127dxUA2AzuBzRUDWVfZlq4YyG6gs9ImdzWQ%2FwFNRlgCFdG5bAAAAABJRU5ErkJggg%3D%3D" alt="Slack-Swung"/></a>
<a href="https://gitter.im/GeoStat-Framework/community"><img src="https://img.shields.io/badge/Gitter-GeoStat--Framework-ed1965?logo=gitter&style=flat" alt="Gitter-GSTools"/></a>
<a href="mailto:info@geostat-framework.org"><img src="https://img.shields.io/badge/Email-GeoStat--Framework-468a88?style=flat&logo=data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSI1MDAiIGhlaWdodD0iNTAwIj48cGF0aCBkPSJNNDQ4IDg4SDUyYy0yNyAwLTQ5IDIyLTQ5IDQ5djIyNmMwIDI3IDIyIDQ5IDQ5IDQ5aDM5NmMyNyAwIDQ5LTIyIDQ5LTQ5VjEzN2MwLTI3LTIyLTQ5LTQ5LTQ5em0xNiA0OXYyMjZsLTIgNy0xMTUtMTE2IDExNy0xMTd6TTM2IDM2M1YxMzdsMTE3IDExN0wzOCAzNzBsLTItN3ptMjE5LTYzYy0zIDMtNyAzLTEwIDBMNjYgMTIxaDM2OHptLTc5LTIzIDQ2IDQ2YTM5IDM5IDAgMCAwIDU2IDBsNDYtNDYgMTAxIDEwMkg3NXoiIHN0eWxlPSJmaWxsOiNmNWY1ZjU7ZmlsbC1vcGFjaXR5OjEiLz48L3N2Zz4=" alt="Email"/></a>
<a href="https://twitter.com/GSFramework"><img alt="Twitter Follow" src="https://img.shields.io/twitter/follow/GSFramework?style=social"></a>
</p>
<p align="center"><b>Get in Touch!</b></p>

The [**GeoStat Framework**](https://geostat-framework.org) is a coherent ecosystem of Python packages for geostatistical applications and subsurface simulations. 
It provides an easily usable open source collection of software packages. They are well documented, including exhaustive hands-on guides and examples for helping non-programmers and non-domain-experts getting started. The main applications of the packages are:
1. [GSTools](https://github.com/GeoStat-Framework/GSTools) & [PyKrige](https://github.com/GeoStat-Framework/PyKrige) - spatial random field generation, kriging, and geostatistical analyses based on variogram methods
2. [ogs5py](https://github.com/GeoStat-Framework/ogs5py) - pre-processing, operating, and post-processing of subsurface flow and transport simulations by providing a Python API for the FEM solver OpenGeoSys 5
3. [AnaFlow](https://github.com/GeoStat-Framework/AnaFlow) & [pentapy](https://github.com/GeoStat-Framework/pentapy) - (semi-)analytical solutions for specific groundwater-flow scenarios
4. [welltestpy](https://github.com/GeoStat-Framework/welltestpy) - store, manipulate, and analyze well-based field testing campaigns with a focus on estimating parameters of subsurface heterogeneity from pumping test data.

With this collection of flexible toolboxes we aim to close the gap of missing software for real-world applications in the field of geostatistics. 
Especially **GSTools** is the first comprehensive Python-toolbox for covariance models, field generation, kriging, variogram estimation, data normalization and transformation. 

The combination of the easy to use and widely adopted programming language Python, together with the extensive documentation and the community building has already come to fruition, as scientists from unrelated fields have successfully contributed novel functionality to the GeoStat Framework.
Not only do the different major versions of the software packages have their own DOIs for reproducibility, but also the larger stand-alone examples.

All packages can be installed via pip and conda on all platforms:
```
pip install gstools pykrige ogs5py welltestpy anaflow pentapy
```
Or:
```
conda install gstools pykrige ogs5py welltestpy anaflow pentapy
```
## GeoStat-Examples
In order to make workflows created with the GeoStat-Framework accessible and reproducible, we have created a companion organization called [**GeoStat-Examples**](https://github.com/GeoStat-Examples), where we provide complex workflows and supplements for publications using packages from the GeoStat-Framework.


## Contact
- [Sebastian Müller](mailto:sebastian.mueller@ufz.de) or [info@geostat-framework.org](mailto:info@geostat-framework.org)

### Co-Authors
- [Dr. Lennart Schüler](mailto:lennart.schueler@ufz.de)
- [Dr. Falk Heße](mailto:falk.hesse@ufz.de)
- [Prof. Alraune Zech](mailto:a.zech@uu.nl)

# GSTools
[![GSTools](https://img.shields.io/badge/GeoStat_Framework-GSTools-468a88?logo=github&style=flat)](https://github.com/GeoStat-Framework/GSTools)
[![GMD](https://img.shields.io/badge/GMD-10.5194%2Fgmd--15--3161--2022-orange)](https://doi.org/10.5194/gmd-15-3161-2022)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1313628.svg)](https://doi.org/10.5281/zenodo.1313628)
[![PyPI version](https://badge.fury.io/py/gstools.svg)](https://badge.fury.io/py/gstools)
[![Documentation Status](https://readthedocs.org/projects/gstools/badge/?version=latest)](https://geostat-framework.readthedocs.io/projects/gstools/en/stable/?badge=stable)

<p align="center">
<img src="{{ site.directory.images | relative_url}}spotlights/geostat/gstools.png" alt="GSTools-LOGO" width="251px"/>
</p>

## Purpose
<img align="right" width="450" src="{{ site.directory.images | relative_url}}spotlights/geostat/demonstrator.png" alt="demonstrator.png">

GeoStatTools provides geostatistical tools for various purposes:
- random field generation
- simple, ordinary, universal and external drift kriging
- conditioned field generation
- incompressible random vector field generation
- (automated) variogram estimation and fitting
- directional variogram estimation and modelling
- data normalization and transformation
- many readily provided and even user-defined covariance models
- metric spatio-temporal modelling
- plotting and exporting routines

See the [documentation](https://gstools.readthedocs.io/).

## Abstract
Geostatistics as a subfield of statistics accounts for the spatial correlations encountered in many applications of, for example, earth sciences. Valuable information can be extracted from these correlations, also helping to address the often encountered burden of data scarcity. Despite the value of additional data, the use of geostatistics still falls short of its potential. This problem is often connected to the lack of user-friendly software hampering the use and application of geostatistics. We therefore present GSTools, a Python-based software suite for solving a wide range of geostatistical problems. We chose Python due to its unique balance between usability, flexibility, and efficiency and due to its adoption in the scientific community. GSTools provides methods for generating random fields; it can perform kriging, variogram estimation and much more. We demonstrate its abilities by virtue of a series of example applications detailing their use.

# PyKrige
[![PyKrige](https://img.shields.io/badge/GeoStat_Framework-PyKrige-468a88?logo=github&style=flat)](https://github.com/GeoStat-Framework/PyKrige)
[![AGU](https://img.shields.io/badge/AGU_2014-H51K--0753-orange)](https://ui.adsabs.harvard.edu/abs/2014AGUFM.H51K0753M)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3738604.svg)](https://doi.org/10.5281/zenodo.3738604)
[![PyPI version](https://badge.fury.io/py/PyKrige.svg)](https://badge.fury.io/py/PyKrige)
[![Documentation Status](https://readthedocs.org/projects/pykrige/badge/?version=stable)](http://pykrige.readthedocs.io/en/stable/?badge=stable)

<p align="center">
<img src="{{ site.directory.images | relative_url}}spotlights/geostat/PyKrige_250.png" alt="PyKrige-LOGO" width="251px"/>
</p>

## Purpose
Kriging Toolkit for Python that was formerly developed independently by [Benjamin S. Murphy](https://github.com/bsmurphy). 
See the [documentation](https://pykrige.readthedocs.io/).

## Abstract
The code supports 2D and 3D ordinary and universal kriging. Standard
variogram models (linear, power, spherical, gaussian, exponential) are
built in, but custom variogram models can also be used. The 2D universal
kriging code currently supports regional-linear, point-logarithmic, and
external drift terms, while the 3D universal kriging code supports a
regional-linear drift term in all three spatial dimensions. Both
universal kriging classes also support generic 'specified' and
'functional' drift capabilities. With the 'specified' drift capability,
the user may manually specify the values of the drift(s) at each data
point and all grid points. With the 'functional' drift capability, the
user may provide callable function(s) of the spatial coordinates that
define the drift(s). The package includes a module that contains
functions that should be useful in working with ASCII grid files (`\*.asc`).

# ogs5py
[![ogs5py](https://img.shields.io/badge/GeoStat_Framework-ogs5py-468a88?logo=github&style=flat)](https://github.com/GeoStat-Framework/ogs5py)
[![NGWA](https://img.shields.io/badge/NGWA-10.1111%2Fgwat.13017-orange)](https://doi.org/10.1111/gwat.13017)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.2546767.svg)](https://doi.org/10.5281/zenodo.2546767)
[![PyPI version](https://badge.fury.io/py/ogs5py.svg)](https://badge.fury.io/py/ogs5py)
[![Documentation Status](https://readthedocs.org/projects/ogs5py/badge/?version=stable)](https://geostat-framework.readthedocs.io/projects/ogs5py/en/stable/?badge=stable)

<p align="center">
<img src="{{ site.directory.images | relative_url}}spotlights/geostat/OGS.png" alt="ogs5py-LOGO" width="251px"/>
</p>

## Purpose
ogs5py is A python-API for the [OpenGeoSys 5][ogs5_link] scientific modeling package.
See the [documentation](https://ogs5py.readthedocs.io/).

## Abstract
High-performance numerical codes are an indispensable tool for hydrogeologists when modeling subsurface flow and transport systems. But as they are written in compiled languages, like C/C++ or Fortran, established software packages are rarely user-friendly, limiting a wider adoption of such tools. OpenGeoSys (OGS), an open-source, finite-element solver for thermo-hydro-mechanical–chemical processes in porous and fractured media, is no exception. Graphical user interfaces may increase usability, but do so at a dramatic reduction of flexibility and are difficult or impossible to integrate into a larger workflow. Python offers an optimal trade-off between these goals by providing a highly flexible, yet comparatively user-friendly environment for software applications. Hence, we introduce ogs5py, a Python-API for the OpenGeoSys 5 scientific modeling package. It provides a fully Python-based representation of an OGS project, a large array of convenience functions for users to interact with OGS and connects OGS to the scientific and computational environment of Python.

# welltestpy
[![welltestpy](https://img.shields.io/badge/GeoStat_Framework-welltestpy-468a88?logo=github&style=flat)](https://github.com/GeoStat-Framework/welltestpy)
[![NGWA](https://img.shields.io/badge/NGWA-10.1111%2Fgwat.13121-orange)](https://doi.org/10.1111/gwat.13121)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1229051.svg)](https://doi.org/10.5281/zenodo.1229051)
[![PyPI version](https://badge.fury.io/py/welltestpy.svg)](https://badge.fury.io/py/welltestpy)
[![Documentation Status](https://readthedocs.org/projects/welltestpy/badge/?version=latest)](https://geostat-framework.readthedocs.io/projects/welltestpy/en/latest/?badge=latest)

<p align="center">
<img src="{{ site.directory.images | relative_url}}spotlights/geostat/WTP.png" alt="welltestpy-LOGO" width="251px"/>
</p>

## Purpose
welltestpy provides a framework to handle, process, plot and analyse data from well based field campaigns.
See the [documentation](https://welltestpy.readthedocs.io/).

## Abstract
The Python package welltestpy provides a workflow to infer parameters of subsurface heterogeneity from pumping test data. It contains routines to handle, visualize, process and interpret field exploration campaigns based on well observations.

# AnaFlow
[![AnaFlow](https://img.shields.io/badge/GeoStat_Framework-AnaFlow-468a88?logo=github&style=flat)](https://github.com/GeoStat-Framework/AnaFlow)
[![AWR](https://img.shields.io/badge/AWR-10.1016%2Fj.advwatres.2021.104027-orange)](https://doi.org/10.1016/j.advwatres.2021.104027)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1135723.svg)](https://doi.org/10.5281/zenodo.1135723)
[![PyPI version](https://badge.fury.io/py/anaflow.svg)](https://badge.fury.io/py/anaflow)
[![Documentation Status](https://readthedocs.org/projects/docs/badge/?version=latest)](https://anaflow.readthedocs.io/en/latest/)

<p align="center">
<img src="{{ site.directory.images | relative_url}}spotlights/geostat/Anaflow.png" alt="AnaFlow-LOGO" width="251px"/>
</p>

## Purpose
AnaFlow provides several analytical and semi-analytical solutions for the groundwater-flow equation.
See the [documentation](https://anaflow.readthedocs.io/).

## Abstract
Pumping tests are established for characterizing spatial average properties of aquifers. At the same time, they are promising tools to identify heterogeneity characteristics such as log-conductivity variance and correlation scales. We present the extended Generalized Radial Flow Model (eGRF) which combines the characterization of well flow in fractal geometry with an upscaled conductivity for pumping tests in heterogeneous media. The eGRF model is the core part of AnaFlow to calculate type curves for different pumping test scenarios under transient conditions.


# pentapy
[![pentapy](https://img.shields.io/badge/GeoStat_Framework-pentapy-468a88?logo=github&style=flat)](https://github.com/GeoStat-Framework/pentapy)
[![status](https://joss.theoj.org/papers/57c3bbdd7b7f3068dd1e669ccbcf107c/status.svg)](https://joss.theoj.org/papers/57c3bbdd7b7f3068dd1e669ccbcf107c)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.2587158.svg)](https://doi.org/10.5281/zenodo.2587158)
[![PyPI version](https://badge.fury.io/py/pentapy.svg)](https://badge.fury.io/py/pentapy)
[![Documentation Status](https://readthedocs.org/projects/pentapy/badge/?version=latest)](https://geostat-framework.readthedocs.io/projects/pentapy/en/latest/?badge=latest)

<p align="center">
<img src="{{ site.directory.images | relative_url}}spotlights/geostat/pentapy.png" alt="pentapy-LOGO" width="251px"/>
</p>

## Purpose

pentapy is a toolbox to deal with pentadiagonal matrices in Python.
See the [documentation](https://pentapy.readthedocs.io/).

## Abstract
Pentadiagonal linear equation systems arise in many areas of science and engineering:
e.g. when solving differential equations, in interpolation problems, or in numerical schemes like finite difference.
A specific example is the radial symmetric groundwater flow equation with
consecutive rings of different constant transmissivity and radial boundary conditions, which
can be expressed as a pentadiagonal equation system.
