---
title: Events Archive
title_image: default
layout: event-list
excerpt:
  "Upcoming and past events that are related to or organized by HIFIS
  Software."
---
{% comment %}
  This markdown file triggers the generation of the event-list page.
  Only the frontmatter is required by Jekyll.
  The contents section does not get rendered into HTML on purpose.
{% endcomment %}
