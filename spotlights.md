---
title: Helmholtz Software Spotlights
title_image: default
layout: spotlight-list
additional_css:
    - services/services-page-images.css
    - spotlights.css
additional_js:
    - ../vendor/jquery/dist/jquery.min.js
    - spotlights_filter.js
excerpt:
  "Outstanding software products by the Helmholtz Centres."
---
{% comment %}
  This markdown file triggers the generation of the spotlights page.
  Only the frontmatter is required by Jekyll.
  The contents section does not get rendered into HTML on purpose.
{% endcomment %}
